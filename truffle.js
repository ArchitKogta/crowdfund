/*
 * NB: since truffle-hdwallet-provider 0.0.5 you must wrap HDWallet providers in a 
 * function when declaring them. Failure to do so will cause commands to hang. ex:
 * ```
 * mainnet: {
 *     provider: function() { 
 *       return new HDWalletProvider(mnemonic, 'https://mainnet.infura.io/<infura-key>') 
 *     },
 *     network_id: '1',
 *     gas: 4500000,
 *     gasPrice: 10000000000,
 *   },
 */

module.exports = {
   networks: {
    development: {
	
     host: "localhost",
     port: 8545,
     network_id: "*", // Match any network id
     from: "0x03b1c7cd8f244c450d71135ce8bdd0697c086c85",
     gasPrice: 100000000000,
     gas: 4712388
    }
  }
};
